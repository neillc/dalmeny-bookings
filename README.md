# Dalmeny Bookings

## Description

A Django 4 based booking system for a holiday house

## Visuals
TBA
## Installation

## Usage

## Support
Email neill.cox@ingeniious.com.au

## Roadmap

Implement goals

## Contributing
Maybe later

## Authors and acknowledgment
Just me so far for code

Jenny Cox for the avatar

## License
MIT License
## Project status
Planning stage at the moment


Goals:
	- to be part of a Website for the Dalmeny house.
	- allow people to request a booking for a period of days
	- to allow admins to approve/disapprove/edit/delete bookings
	
	
Stretch Goals:
	- wait lists
	- payment processing
	

